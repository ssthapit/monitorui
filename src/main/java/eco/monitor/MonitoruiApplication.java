package eco.monitor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitoruiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoruiApplication.class, args);
	}

}
